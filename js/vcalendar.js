if (Drupal.jsEnabled) {
  $(document).ready(function(){
    // Ready code here.
    //display_vcalendar_text($('.vcalendar-toggle:checked').val());
    //display_vcalendar_text($('input[name=vcalendar_toggle]:checked').val());
    $('.vcalendar-toggle').click(function () {
      display_vcalendar_text($(this).val());
    });
  });
}

function display_vcalendar_text(value)  {
  switch(value) {
    case 'yes':
      $('#edit-vcalendar-message-wrapper').slideDown(500);
    break;

    case 'no':
      $('#edit-vcalendar-message-wrapper').slideUp(500);
    break;
  }
}
